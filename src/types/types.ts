export interface User {
  avatar_url: string;
  id: number;
  login: string;
  url: string;
  name?: string;
  followers?: number;
  following?: number;
  selected?: boolean;
  deleted?: boolean;
  github_search?: boolean;
}
