import React, { FC, useEffect, useState } from 'react';
import Alert from '@mui/material/Alert';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { Header } from '../Header/Header';
import { UsersList } from '../UsersList/UsersList';
import { User } from '../../types/types';
import UserService from '../../service/UserService';

export const UsersPage: FC = () => {
  const [usersList, setUsersList] = useState<User[]>([]);
  const [searchList, setSearchList] = useState<User[]>([]);
  const [checkboxValue, setCheckboxValue] = useState(false);
  const [searchValue, setSearchValue] = useState('');
  const [loading, setLoading] = useState(false);
  const [isFiltered, setIsFiltered] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    setLoading(true);
    UserService.fetchUsersList()
      .then((res) => {
        setUsersList(res);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  useEffect(() => {
    onSearch(searchValue);
    setShowAlert(checkboxValue && !!searchValue);
  }, [checkboxValue, searchValue]);

  const data = () => {
    if (searchValue && isFiltered) return searchList?.filter((elem) => elem?.selected);
    if (searchValue) return searchList;
    if (isFiltered) return usersList?.filter((elem) => elem?.selected);
    return usersList;
  };

  const deleteUser = (id: number) => {
    const deleted: User[] = usersList?.map((elem) => {
      if (elem?.id === id) return { ...elem, deleted: !elem?.deleted };
      return elem;
    });
    setUsersList(deleted);

    if (searchValue) {
      const marked: User[] = searchList?.map((elem) => {
        if (elem?.id === id) return { ...elem, deleted: !elem?.deleted };
        return elem;
      });
      setSearchList(marked);
    }
  };

  const selectUser = (id: number) => {
    const marked: User[] = usersList?.map((elem) => {
      if (elem?.id === id) return { ...elem, selected: !elem?.selected };
      return elem;
    });
    setUsersList(marked);

    if (searchValue) {
      const marked: User[] = searchList?.map((elem) => {
        if (elem?.id === id) return { ...elem, selected: !elem?.selected };
        return elem;
      });
      setSearchList(marked);
    }
  };

  const onSearch = (value: string) => {
    if (!value) {
      setSearchList([]);
      return;
    }

    if (checkboxValue) {
      setLoading(true);
      UserService.fetchQueryUsersList(value)
        .then((result: User[]) => {
          setSearchList(result?.map((elem) => ({ ...elem, github_search: true })));
        })
        .finally(() => {
          setLoading(false);
        });
      return;
    }

    setSearchList(usersList?.filter((elem) => elem?.login?.includes(value)));
  };

  return (
    <>
      <main>
        <Header
          isFiltered={isFiltered}
          setIsFiltered={setIsFiltered}
          searchValue={searchValue}
          setSearchValue={setSearchValue}
          checkboxValue={checkboxValue}
          setCheckboxValue={setCheckboxValue}
        />
        <div className="container">
          {showAlert && (
            <Alert
              icon={<ErrorOutlineIcon sx={{ color: '#8c1700b0', fontSize: '23px' }} />}
              sx={{ mb: 4, backgroundColor: '#ff553326', color: '#8c1700b0', fontSize: '16px' }}
            >
              Пользователи из результатов поиска в GitHub не могут быть удалены или добавлены в избранное
            </Alert>
          )}
          {loading ? (
            <h1 className="title">Загрузка пользователей...</h1>
          ) : (
            <UsersList usersList={data()} deleteUser={deleteUser} selectUser={selectUser} />
          )}
        </div>
      </main>
    </>
  );
};
