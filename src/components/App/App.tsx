import React, { FC } from 'react';
import { UsersPage } from '../UsersPage/UsersPage';

export const App: FC = () => {
  return <UsersPage />;
};
