import React, { FC } from 'react';
import { getFollowerWordEnding, getFollowingWordEnding, getShortNotation } from '../../service/helper';
import { User } from '../../types/types';

import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';

import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import StarIcon from '@mui/icons-material/Star';

interface Props {
  usersList: User[];
  deleteUser: (id: number) => void;
  selectUser: (id: number) => void;
}

export const UsersList: FC<Props> = ({ usersList, deleteUser, selectUser }) => {
  return (
    <>
      {usersList?.length ? (
        <Grid container spacing={4}>
          {usersList.map(
            ({ id, avatar_url, login, name, followers, following, selected, deleted, github_search }) =>
              !deleted && (
                <Grid item xs={4} key={id}>
                  <Card sx={{ minWidth: 300, maxWidth: 400 }}>
                    <CardHeader
                      avatar={<Avatar src={avatar_url} sx={{ width: 100, height: 100 }} />}
                      title={login}
                      titleTypographyProps={{ fontSize: '30px' }}
                      subheader={name}
                      subheaderTypographyProps={{ fontSize: '20px' }}
                    />
                    <CardContent sx={{ display: 'flex', flexFlow: 'row wrap', justifyContent: 'space-evenly' }}>
                      <Box sx={{ display: 'flex', flexDirection: 'row' }}>
                        <Typography sx={{ color: '#f53', mr: 0.5, fontSize: '18px', fontWeight: '700' }}>
                          {getShortNotation(followers)}
                        </Typography>
                        <Typography sx={{ fontSize: '18px' }}>{getFollowerWordEnding(followers)} </Typography>
                      </Box>

                      <Box sx={{ display: 'flex', flexDirection: 'row' }}>
                        <Typography sx={{ color: '#f53', mr: 0.5, fontSize: '18px', fontWeight: '700' }}>
                          {getShortNotation(following)}
                        </Typography>
                        <Typography sx={{ fontSize: '18px' }}>{getFollowingWordEnding(following)}</Typography>
                      </Box>
                    </CardContent>
                    {!github_search && (
                      <CardActions sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                        <IconButton sx={{ color: '#f53' }} onClick={() => selectUser(id)}>
                          {selected ? (
                            <StarIcon sx={{ color: '#f53' }} fontSize="large" />
                          ) : (
                            <StarBorderIcon sx={{ color: '#f53' }} fontSize="large" />
                          )}
                        </IconButton>

                        <IconButton sx={{ color: '#f53' }} onClick={() => deleteUser(id)}>
                          <DeleteOutlineIcon sx={{ color: '#f53' }} fontSize="large" />
                        </IconButton>
                      </CardActions>
                    )}
                  </Card>
                </Grid>
              )
          )}
        </Grid>
      ) : (
        <h1 className="title">Пользователи не найдены...</h1>
      )}
    </>
  );
};
