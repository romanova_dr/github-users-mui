import React, { FC } from 'react';
import { styled, alpha } from '@mui/material/styles';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import Toolbar from '@mui/material/Toolbar';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';

import StarBorderIcon from '@mui/icons-material/StarBorder';
import StarIcon from '@mui/icons-material/Star';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 1),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '16ch',
      '&:focus': {
        width: '30ch',
      },
    },
  },
}));

interface Props {
  isFiltered: boolean;
  setIsFiltered: (value: boolean) => void;
  searchValue: string;
  setSearchValue: (value: string) => void;
  checkboxValue: boolean;
  setCheckboxValue: (value: boolean) => void;
}

export const Header: FC<Props> = ({
  isFiltered,
  setIsFiltered,
  searchValue,
  setSearchValue,
  checkboxValue,
  setCheckboxValue,
}) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" sx={{ backgroundColor: '#f53' }}>
        <Toolbar>
          <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}>
            Пользователи GitHub
          </Typography>
          <Search>
            <StyledInputBase
              placeholder="Найти…"
              inputProps={{ 'aria-label': 'search' }}
              value={searchValue}
              onChange={(event) => setSearchValue(event.currentTarget.value)}
            />
            <Tooltip title="Поиск пользователей в GitHub">
              <FormControlLabel
                control={
                  <Checkbox
                    sx={{
                      color: '#fff',
                      '&.Mui-checked': {
                        color: '#fff',
                      },
                    }}
                    value={checkboxValue}
                    onChange={(event) => setCheckboxValue(event.currentTarget.checked)}
                  />
                }
                label="GitHub"
              />
            </Tooltip>
          </Search>
          <Tooltip title="Избранное">
            <IconButton sx={{ ml: 2 }} onClick={() => setIsFiltered(!isFiltered)}>
              {isFiltered ? <StarIcon sx={{ color: '#fff' }} /> : <StarBorderIcon sx={{ color: '#fff' }} />}
            </IconButton>
          </Tooltip>
        </Toolbar>
      </AppBar>
    </Box>
  );
};
